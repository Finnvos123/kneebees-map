Kneebees Map
====================
Kneebees interactive map with proper names - https://kneebees.netlify.com

Created by Finn

Repository for the map tiles can be found here [Sea Of Thieves Map Map](https://github.com/Chenzo/sea-of-thieves-map-map)

Credits & Licence
--------------------------------------
Based on the interactive map by [Chenzo](https://github.com/Chenzo/sea-of-thieves-map).

The Sea Of Thieves icons &amp; map are the property of [RARE](https://www.seaofthieves.com/).

Map tiles are edited versions of Chenzo's tiles, the original repository can be found [here](https://github.com/Chenzo/sea-of-thieves-map-map).

This software uses the following third party libraries, licensed seperately:
* [Leaflet](http://leafletjs.com)
* [gdal2tiles-leaflet](https://github.com/commenthol/gdal2tiles-leaflet)
* [Leaflet.SimpleGraticule](https://github.com/ablakey/Leaflet.SimpleGraticule)
* [leaflet-search](https://github.com/stefanocudini/leaflet-search)
